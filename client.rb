#!/usr/bin/env ruby -w
require "socket"
require 'bundler/setup'
require 'gpio'

class Client
  def initialize( server )
    @server = server
    @request = nil
    @response = nil
    @name = "Desk lamp"
    @status = 0;
    @ssr = GPIO::OutputPin.new(pin: 11)
    listen
    send

    @request.join
    @response.join
  end
 
  def listen
    @response = Thread.new do
      loop {
        msg = @server.gets.chomp
        puts "#{msg}"

        # Parse message and check if a command was sent, act accordingly
        parse = msg.delete(' ').split(",")
        if parse[0] == "command" && parse.count > 1
          runCommand(parse[1])
        end

      }
    end
  end

  def runCommand(command)
      puts "Attempting to run command: " + command

      if command == "on"
        # Turn pin on
        @ssr.on
        @status = 1
        puts "Turned device on"
        status()
        
      elsif command == "off"
        # Turn pin off
        @ssr.off
        @status = 0
        puts "Turned device off"
        status()

      elsif command == "status"
        # Return status
        puts "Sending back status"
        status()
        
      elsif command == "ping"
        # Ping is for the server to check if the device is alive
        puts "Sending back a ping"
        @server.puts("message, ping")
      end

  end

  def status ()
    #Check status of pin and send to server
    puts "Sending back status"
    @server.puts("status,  #{@status}")
  end
 
  def send
    @request = Thread.new do
        @server.puts(@name)
      loop {
        msg = $stdin.gets.chomp
        @server.puts( msg )
      }
    end
  end
end
 
server = TCPSocket.open( "ssr.pxy.dk", 2000 )
Client.new( server )